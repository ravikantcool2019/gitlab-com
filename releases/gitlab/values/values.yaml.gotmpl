# Default values for gitlab/gitlab chart

## NOTICE
# Due to the scope and complexity of this chart, all possible values are
# not documented in this file. Extensive documentation for these values
# and more can be found at https://gitlab.com/charts/gitlab/

## Advanced Configuration
# Documentation for advanced configuration can be found under doc/advanced
# - external PostgreSQL
# - external Gitaly
# - external Redis
# - external NGINX
# - PersistentVolume configuration
# - external Object Storage providers

## The global properties are used to configure multiple charts at once.
## Extended documenation at doc/charts/globals.md
---

global:
  ## GitLab operator is Alpha. Not for production use.
  operator:
    enabled: false

  ## Supplemental Pod labels. Will not be used for selectors
  pod:
    labels:
      stage: {{ .Environment.Values | getOrNil "stage" | default "main" }}

  ## doc/installation/deployment.md#deploy-the-community-edition
  # edition: ee

  ## doc/charts/globals.md#gitlab-version
  # `gitlabVersion` is set via init-values.yaml.gotmpl
  # gitlabVersion:

  ## doc/charts/globals.md#application-resource
  application:
    create: false
    links: []
    allowClusterRoles: true
  ## doc/charts/globals.md#configure-host-settings
  hosts:
    # hostSuffix:
    https: true
    ssh: ~
    gitlab:
      https: true
    registry:
      https: true

  ## doc/charts/globals.md#configure-ingress-settings
  ingress:
    configureCertmanager: false
    annotations: {}
    enabled: false
    tls:
      enabled: true

  ## Initial root password for this GitLab installation
  ## Secret created according to doc/installation/secrets.md#initial-root-password
  ## If allowing shared-secrets generation, this is OPTIONAL.
  initialRootPassword: {}
    # secret: RELEASE-gitlab-initial-root-password
    # key: password

  ## doc/installation/secrets.md#gitlab-kas-secret
  kas:
    enabled: false

  ## doc/charts/globals.md#configure-postgresql-settings
  psql:
    connectTimeout: 5 # 5 seconds
    password:
      secret: gitlab-postgres-credential-v1
      key: secret
    # host: postgresql.hostedsomewhere.else
    # port: 123
    # username: gitlab
    # database: gitlabhq_production

  ## doc/charts/globals.md#configure-redis-settings
  redis:
    password:
      enabled: true
      secret: gitlab-redis-credential-v1
      key: secret
    # host: redis.hostedsomewhere.else
    # port: 6379

  ## doc/charts/globals.md#configure-gitaly-settings
  gitaly:
    enabled: false
    authToken:
      secret: gitlab-gitaly-credential-v1
      key: secret
    internal:
      names: []
    external: []

  ## doc/charts/globals.md#configure-minio-settings
  minio:
    enabled: false
    credentials: {}
      # secret:

  ## doc/charts/globals.md#configure-appconfig-settings
  ## Rails based portions of this chart share many settings
  appConfig:
    ## doc/charts/globals.md#general-application-settings
    enableUsagePing:
    enableImpersonation:
    defaultCanCreateGroup: true
    usernameChangingEnabled: true
    issueClosingPattern:
    defaultTheme:
    defaultProjectsFeatures:
      issues:
      mergeRequests:
      wiki:
      snippets:
      builds:
    webhookTimeout: 10
    gitlab_kas:
      secret: gitlab-kas-credential-v1

    ## doc/charts/globals.md#cron-jobs-related-settings
    cron_jobs:
      stuck_ci_jobs_worker:
        cron: "0 * * * *"
      pipeline_schedule_worker:
        cron: "*/5 * * * *"
      expire_build_artifacts_worker:
        cron: "50 * * * *"
      repository_check_worker:
        cron: "20 * * * *"
      admin_email_worker:
        cron: "0 0 * * 0"
      repository_archive_cache_worker:
        cron: "30 * * * *"
      pages_domain_verification_cron_worker:
        cron: "*/15 * * * *"
      pseudonymizer_worker:
        cron: "0 23 * * *"
      schedule_migrate_external_diffs_worker:
        cron: "15 * * * *"

    ## doc/charts/globals.md#gravatarlibravatar-settings
    gravatar:
      plainUrl:
      sslUrl:

    ## doc/charts/globals.md#hooking-analytics-services-to-the-gitlab-instance
    extra:
      googleAnalyticsId:
      piwikUrl:
      piwikSiteId:

    ## doc/charts/globals.md#lfs-artifacts-uploads-packages-external-mr-diffs
    lfs:
      enabled: false
      proxy_download: true
      connection:
        secret: gitlab-object-storage-v1
        key: gitlab-object-storage.yml
    artifacts:
      enabled: false
      proxy_download: false
      connection:
        secret: gitlab-object-storage-v1
        key: gitlab-object-storage.yml
    uploads:
      enabled: false
      proxy_download: true
      connection:
        secret: gitlab-object-storage-v1
        key: gitlab-object-storage.yml
    packages:
      enabled: false
      proxy_download: true
      connection:
        secret: gitlab-object-storage-v1
        key: gitlab-object-storage.yml
    externalDiffs:
      enabled: false
      proxy_download: true
      connection:
        secret: gitlab-object-storage-v1
        key: gitlab-object-storage.yml
    terraformState:
      enabled: false
      connection:
        secret: gitlab-object-storage-v1
        key: gitlab-object-storage.yml
    dependencyProxy:
      enabled: false
      proxy_download: false
      connection:
        secret: gitlab-object-storage-v1
        key: gitlab-object-storage.yml

    ## doc/charts/globals.md#pseudonymizer-settings
    pseudonymizer:
      configMap:
      bucket: ''
      connection: {}
        # secret:
        # key:
    backups:
      bucket: ''
      tmpBucket: tmp

    ## doc/charts/globals.md#incoming-email-settings
    ## doc/installation/deployment.md#incoming-email
    incomingEmail:
      enabled: false
      address: ""
      host: "imap.gmail.com"
      port: 993
      ssl: true
      startTls: false
      user: ""
      password:
        secret: gitlab-mailroom-imap-v1
        key: incoming_email_password
      expungeDeleted: true
      mailbox: inbox
      idleTimeout: 60

    serviceDeskEmail:
      enabled: false
      address: ""
      host: "imap.gmail.com"
      port: 993
      ssl: true
      startTls: false
      user: ""
      password:
        secret: gitlab-mailroom-imap-v1
        key: service_desk_email_password
      expungeDeleted: true
      mailbox: inbox
      idleTimeout: 60

    ## doc/charts/globals.md#ldap
    ldap:
      servers: {}
      ## 'main' is the GitLab 'provider ID' of this LDAP server
      # main:
      #   label: 'LDAP'
      #   host: '_your_ldap_server'
      #   port: 636
      #   uid: 'sAMAccountName'
      #   bind_dn: '_the_full_dn_of_the_user_you_will_bind_with'
      #   password:
      #     secret: _the_secret_containing_your_ldap_password
      #     key: _the_key_which_holds_your_ldap_password
      #   encryption: 'plain'

    ## doc/charts/globals.md#omniauth
    omniauth:
      enabled: false
      autoSignInWithProvider:
      syncProfileFromProvider: []
      syncProfileAttributes: ['email']
      allowSingleSignOn: true
      blockAutoCreatedUsers: true
      autoLinkLdapUser: false
      autoLinkSamlUser: false
      externalProviders: []
      providers: []
      # - secret: gitlab-google-oauth2
      #   key: provider
  ## End of global.appConfig

  serviceAccount:
    enabled: true
    create: true

  ## doc/charts/globals.md#configure-gitlab-shell-settings
  shell:
    authToken:
      secret: gitlab-shell-credential-v1
      key: secret
    hostKeys:
      secret: gitlab-hostkeys-credential-v1

  ## Rails application secrets
  ## Secret created according to doc/installation/secrets.md#gitlab-rails-secret
  ## If allowing shared-secrets generation, this is OPTIONAL.
  railsSecrets:
    secret: gitlab-rails-secret-v1

  ## doc/charts/globals.md#configure-registry-settings
  registry:
    authEndPoint: {{ env "GITLAB_ENDPOINT" | default (.Environment.Values | getOrNil "gitlab_endpoint") }}
    certificate:
      secret: registry-certificate-v1
    httpSecret:
      secret: registry-httpsecret-v1
    notifications:
      endpoints:
        - name: gitlab
          backoff: 1s
          headers:
            Authorization:
              secret: gitlab-registry-authorization-header-v1
              key: secret
          ignore:
            actions:
              - pull
          ignoredmediatypes:
            - application/octet-stream
          threshold: 5
          timeout: 500ms
          url: {{ env "GITLAB_ENDPOINT" | default (.Environment.Values | getOrNil "gitlab_endpoint") }}/api/v4/container_registry_event/events

  ## GitLab Runner
  ## Secret created according to doc/installation/secrets.md#gitlab-runner-secret
  ## If allowing shared-secrets generation, this is OPTIONAL.
  runner:
    registrationToken: {}
      # secret:

  ## doc/installation/deployment.md#outgoing-email
  ## Outgoing email server settings
  smtp:
    enabled: false
    address: smtp.mailgun.org
    port: 2525
    user_name: ""
    ## doc/installation/secrets.md#smtp-password
    password:
      secret: ""
      key: password
    # domain:
    authentication: "plain"
    starttls_auto: false
    openssl_verify_mode: "peer"

  ## doc/installation/deployment.md#outgoing-email
  ## Email persona used in email sent by GitLab
  email:
    from: ''
    display_name: GitLab
    reply_to: ''
    subject_suffix: ''

  ## Timezone for containers.
  time_zone:

  ## Global Service Annotations
  service:
    annotations: {}

  antiAffinity: soft

  ## doc/installation/secrets.md#gitlab-workhorse-secret
  workhorse:
    secret: gitlab-workhorse-credential-v1
    key: secret

  ## doc/charts/globals.md#custom-certificate-authorities
  # configuration of certificates container & custom CA injection
  certificates:
    image:
      repository: dev.gitlab.org:5005/gitlab/charts/components/images/alpine-certificates
      tag: 20171114-r3
    customCAs: []
    # - secret: custom-CA
    # - secret: more-custom-CAs
## End of global

## Settings to for the Let's Encrypt ACME Issuer
# certmanager-issuer:
  ## The email address to register certificates requested from Let's Encrypt.
  ## Required if using Let's Encrypt.

## Installation & configuration of stable/cert-manager
## See requirements.yaml for current version
certmanager:
  # Install cert-manager chart. Set to false if you already have cert-manager
  # installed or if you are not using cert-manager.
  install: false
  # Other cert-manager configurations from upstream
  # See https://github.com/kubernetes/charts/tree/master/stable/cert-manager#configuration
  rbac:
    create: true

## doc/charts/nginx/index.md
## doc/architecture/decisions.md#nginx-ingress
## Installation & configuration of charts/nginx
nginx-ingress:
  enabled: true
  tcpExternalConfig: "true"
  controller:
    nodeSelector:
      type: default
    autoscaling:
      enabled: true
      minReplicas: 1
      maxReplicas: 20
      targetCPUUtilizationPercentage: 50
      targetMemoryUtilizationPercentage: 50
    config:
      hsts-include-subdomains: "false"
      server-name-hash-bucket-size: "256"
      enable-vts-status: "true"
      use-http2: "false"
      ssl-ciphers: "ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES128-SHA:AES256-GCM-SHA384:AES128-GCM-SHA256:AES256-SHA256:AES128-SHA256:AES256-SHA:AES128-SHA:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!PSK:!RC4"
      ssl-protocols: "TLSv1.1 TLSv1.2"
      server-tokens: "false"
    extraArgs:
      force-namespace-isolation: ""
    service:
      externalTrafficPolicy: "Local"
      omitClusterIP: true
      annotations:
        cloud.google.com/load-balancer-type: Internal
    resources:
      requests:
        cpu: 100m
        memory: 100Mi
    publishService:
      enabled: true
    replicaCount: 3
    minAvailable: 2
    scope:
      enabled: true
    stats:
      enabled: true
      service:
        omitClusterIP: true
    metrics:
      enabled: true
      service:
        annotations:
          prometheus.io/scrape: "true"
          prometheus.io/port: "10254"
        omitClusterIP: true
  defaultBackend:
    minAvailable: 1
    replicaCount: 2
    resources:
      requests:
        cpu: 5m
        memory: 5Mi
    service:
      omitClusterIP: true
  rbac:
    create: true
  serviceAccount:
    create: true

## Installation & configuration of stable/prometheus
## See requirements.yaml for current version
prometheus:
  install: false
  rbac:
    create: true
  alertmanager:
    enabled: false
  alertmanagerFiles:
    alertmanager.yml: {}
  kubeStateMetrics:
    enabled: false
  nodeExporter:
    enabled: false
  pushgateway:
    enabled: false

## Configuration of Redis
## doc/architecture/decisions.md#redis
## doc/charts/redis
redis:
  install: false

## Instllation & configuration of stable/prostgresql
## See requirements.yaml for current version
postgresql:
  install: false
  postgresUser: gitlab
  postgresDatabase: gitlabhq_production
  imageTag: 9.6.8
  usePasswordFile: true
  existingSecret: 'secret'
  metrics:
    enabled: true
    ## Optionally define additional custom metrics
    ## ref: https://github.com/wrouesnel/postgres_exporter#adding-new-metrics-via-a-config-file

## Installation & configuration charts/registry
## doc/architecture/decisions.md#registry
## doc/charts/registry/
registry:
  enabled: {{ .Environment.Values.registry.enabled }}
  nodeSelector:
    type: default
  podLabels:
    type: registry
    tier: sv
  image:
    # `tag` is set via init-values.yaml.gotmpl
    # tag:
    repository: dev.gitlab.org:5005/gitlab/charts/components/images/gitlab-container-registry
    pullSecrets:
      - name: dev-registry-access-v1
  hpa:
    cpu:
      targetAverageUtilization: 80
  draintimeout: '60s'
  deployment:
    readinessProbe:
      path: /debug/health
      port: 5001
      # TODO: Consider removal after https://gitlab.com/gitlab-org/container-registry/issues/37
      initialDelaySeconds: 40
    livenessProbe:
      path: /debug/health
      port: 5001
  networkpolicy:
    enabled: true
    egress:
      enabled: true
      # The following rules enable traffic to all external
      # endpoints, except the metadata service and the local
      # network (except DNS requests)
      rules:
      - to:
        - ipBlock:
            cidr: 10.0.0.0/8
        ports:
        - port: 53
          protocol: UDP
        - port: 53
          protocol: TCP
      - to:
        - ipBlock:
            cidr: 0.0.0.0/0
            except:
            - 10.0.0.0/8
            - 169.254.169.254/32
  resources:
    limits:
      memory: 3G
  debug:
    addr:
      port: 5001
    prometheus:
      enabled: true
      path: '/metrics'
  ingress:
    enabled: false
  log:
    formatter: json
  service:
    type: LoadBalancer
    annotations:
      cloud.google.com/load-balancer-type: Internal
  storage:
    secret: registry-storage-v1
    key: config
    extraKey: gcs.json
  tokenIssuer: omnibus-gitlab-issuer
  health:
    storagedriver:
      enabled: true
      interval: '10s'
      threshold: 3
  profiling:
    stackdriver:
      enabled: true
  serviceAccount:
    enabled: true
    create: true
    annotations:
      iam.gke.io/gcp-service-account: gitlab-registry@{{ .Environment.Values.google_project }}.iam.gserviceaccount.com

## Automatic shared secret generation
## doc/installation/secrets.md
## doc/charts/shared-secrets
shared-secrets:
  enabled: false
  rbac:
    create: true

## Installation & configuration of gitlab/gitlab-runner
## See requirements.yaml for current version
gitlab-runner:
  install: false
  rbac:
    create: true
  runners:
    locked: false
    cache:
      cacheType: s3
      s3BucketName: runner-cache
      cacheShared: true
      s3BucketLocation: us-east-1
      s3CachePath: gitlab-runner
      s3CacheInsecure: false

## Settings for individual sub-charts under GitLab
## Note: Many of these settings are configurable via globals
gitlab:
  gitlab-shell:
    enabled: {{ index .Environment.Values.gitlab "gitlab-shell" "enabled" }}
    global:
      shell:
        port: 2222
  # doc/charts/gitlab/migrations
  migrations:
    enabled: false
  # doc/charts/gitlab/webservice
  webservice:
    enabled: {{ .Environment.Values.gitlab.webservice.enabled }}
    shutdown:
      blackoutSeconds: 240
    deployment:
      terminationGracePeriodSeconds: 260
    serviceAccount:
      annotations:
        iam.gke.io/gcp-service-account: gitlab-webservice@{{ .Environment.Values.google_project }}.iam.gserviceaccount.com
    serviceLabels:
      railsPromJob: gitlab-rails
      workhorsePromJob: gitlab-workhorse-git
    trusted_proxies: ['10.0.0.0/8']
    monitoring:
      exporter:
        enabled: true
    ingress:
      annotations:
        ## This is disabled for webservice for git-https traffic
        ## See https://gitlab.com/gitlab-org/charts/gitlab/-/issues/2262 and
        ##     https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1146
        ##
        ## https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/annotations/
        nginx.ingress.kubernetes.io/proxy-request-buffering: "off"
        nginx.ingress.kubernetes.io/proxy-connect-timeout: 300
        nginx.ingress.kubernetes.io/proxy-read-timeout: 3600
    nodeSelector:
      type: default
    podLabels:
      type: git
      tier: sv
    image:
      repository: dev.gitlab.org:5005/gitlab/charts/components/images/gitlab-webservice-ee
      pullSecrets:
      - name: dev-registry-access-v1
    networkpolicy:
      enabled: true
      egress:
        enabled: true
        # The following rules enable traffic to all external
        # endpoints, except the metadata service and the local
        # network (except DNS, gitaly, redis and postgres)
        rules:
          # Allow all traffic except internal network and metadata service
        - to:
          - ipBlock:
              cidr: 0.0.0.0/0
              except:
              - 10.0.0.0/8
              - 169.254.169.254/32
          # Allow internal traffic to consul for consul DNS
        - to:
          - namespaceSelector: {}
            podSelector:
              matchLabels:
                app: consul
          ports:
          - port: 8600
            protocol: TCP
          - port: 8600
            protocol: UDP
          # Allow internal traffic for DNS
        - to:
          - ipBlock:
              cidr: 10.0.0.0/8
          ports:
          - port: 53
            protocol: UDP
          - port: 53
            protocol: TCP

          # Allow internal traffic to Redis
        - to:
          - ipBlock:
              cidr: 10.0.0.0/8
          ports:
            # pre Memorystore instance
          - port: 6379
            protocol: TCP
            # gstg, gprd Redis
          - port: 26379
            protocol: TCP

          # Allow internal traffic to Postgresql
        - to:
          - ipBlock:
              cidr: 10.0.0.0/8
          ports:
            # pre CloudSQL
          - port: 5432
            protocol: TCP
            # gstg, gprd pgbouncer
          - port: 6432
            protocol: TCP
          - port: 6433
            protocol: TCP
          - port: 6434
            protocol: TCP

          # Allow internal traffic to Gitaly
        - to:
          - ipBlock:
              cidr: 10.0.0.0/8
          ports:
          # Gitaly non-TLS
          - port: 9999
            protocol: TCP
          # Gitaly TLS
          - port: 9998
            protocol: TCP
          # Praefect
          - port: 2305
            protocol: TCP

    puma:
      workerMaxMemory: 1342 # in MB units
      threads:
        min: 1
        max: 4
      disableWorkerKiller: false
    workhorse:
      # This needs to be set here until we address
      # logformat: https://gitlab.com/gitlab-org/charts/gitlab/-/issues/2249
      extraArgs: '-logFormat json'
      image: dev.gitlab.org:5005/gitlab/charts/components/images/gitlab-workhorse-ee
      resources:
        requests:
          cpu: 300m
          memory: 100M
      monitoring:
        exporter:
          enabled: true
    resources:
      limits:
        cpu: 2
        memory: 3G
      requests:
        cpu: 2
        memory: 2.5G
    minReplicas: 2
    maxReplicas: 10
    extraEnv:
      ACTION_CABLE_IN_APP: true
      BYPASS_SCHEMA_VERSION: "true"
      GITLAB_CONTINUOUS_PROFILING: stackdriver?service=workhorse-git
      STACKPROF_ENABLED: "true"
      UNSTRUCTURED_RAILS_LOG: "false"
      GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"git\", \"stage\": \"main\"}"
  # doc/charts/gitlab/sidekiq
  sidekiq:
    enabled: {{ .Environment.Values.gitlab.sidekiq.enabled }}
    cluster: true
    trusted_proxies: ['10.0.0.0/8']
    experimentalQueueSelector: true
    memoryKiller:
      maxRss: 0
    nodeSelector:
      type: sidekiq
    image:
      repository: dev.gitlab.org:5005/gitlab/charts/components/images/gitlab-sidekiq-ee
      pullSecrets:
      - name: dev-registry-access-v1
    metrics:
      enabled: true
      port: 8083
    logging:
      format: "json"
    extraEnv:
      BYPASS_SCHEMA_VERSION: "true"
      STACKPROF_ENABLED: "true"
      # This will enables logs for sidekiq args
      # and does not yet have explicit support in
      # the GitLab chart
      # https://gitlab.com/gitlab-org/charts/gitlab/-/issues/2100
      SIDEKIQ_LOG_ARGUMENTS: "1"
    networkpolicy:
      enabled: true
      egress:
        enabled: true
        # The following rules enable traffic to all external
        # endpoints, except the metadata service and the local
        # network (except DNS, gitaly, redis and postgres)
        rules:
          # Allow all traffic except internal network and metadata service
        - to:
          - ipBlock:
              cidr: 0.0.0.0/0
              except:
              - 10.0.0.0/8
              - 169.254.169.254/32
          # Allow internal traffic to consul for consul DNS
        - to:
          - namespaceSelector: {}
            podSelector:
              matchLabels:
                app: consul
          ports:
          - port: 8600
            protocol: TCP
          - port: 8600
            protocol: UDP
          # Allow internal traffic for DNS
        - to:
          - ipBlock:
              cidr: 10.0.0.0/8
          ports:
          - port: 53
            protocol: UDP
          - port: 53
            protocol: TCP

          # Allow internal traffic to Redis
        - to:
          - ipBlock:
              cidr: 10.0.0.0/8
          ports:
            # pre Memorystore instance
          - port: 6379
            protocol: TCP
            # gstg, gprd Redis
          - port: 26379
            protocol: TCP

          # Allow internal traffic to Postgresql
        - to:
          - ipBlock:
              cidr: 10.0.0.0/8
          ports:
            # pre CloudSQL
          - port: 5432
            protocol: TCP
            # gstg, gprd pgbouncer
          - port: 6432
            protocol: TCP

          # Allow internal traffic to Gitaly
        - to:
          - ipBlock:
              cidr: 10.0.0.0/8
          ports:
          # Gitaly non-TLS
          - port: 9999
            protocol: TCP
          # Gitaly TLS
          - port: 9998
            protocol: TCP
          # Praefect
          - port: 2305
            protocol: TCP

          # Allow internal traffic to Container Registry
        - to:
          - ipBlock:
              cidr: 10.0.0.0/8
          ports:
          # Container Registry
          - port: 5000
            protocol: TCP

    podLabels:
      tier: sv
      type: sidekiq

    hpa:
      # Changed from default for urgent-other
      # https://gitlab.com/gitlab-com/gl-infra/production/-/issues/2254#note_358815700
      targetAverageValue: 450m

    pods:
      - name: catchall
        concurrency: 15
        minReplicas: 1
        maxReplicas: 100
        nodeSelector:
          type: catchall
        podLabels:
          shard: catchall
        queues: |
          name=default|name=adjourned_project_deletion|name=admin_emails|name=authorized_project_update:authorized_project_update_project_create|name=authorized_project_update:authorized_project_update_project_group_link_create|name=authorized_project_update:authorized_project_update_user_refresh_over_user_range|name=authorized_project_update:authorized_project_update_user_refresh_with_low_urgency|name=ci_batch_reset_minutes|name=container_repository:cleanup_container_repository|name=container_repository:delete_container_repository|name=cronjob:adjourned_group_deletion|name=cronjob:adjourned_projects_deletion_cron|name=cronjob:clear_shared_runners_minutes|name=cronjob:container_expiration_policy|name=cronjob:environments_auto_stop_cron|name=cronjob:geo_container_repository_sync_dispatch|name=cronjob:geo_file_download_dispatch|name=cronjob:geo_metrics_update|name=cronjob:geo_prune_event_log|name=cronjob:geo_registry_sync|name=cronjob:geo_repository_sync|name=cronjob:geo_repository_verification_primary_batch|name=cronjob:geo_repository_verification_secondary_scheduler|name=cronjob:geo_repository_verification_secondary_shard|name=cronjob:geo_scheduler_per_shard_scheduler|name=cronjob:geo_scheduler_primary_per_shard_scheduler|name=cronjob:geo_scheduler_secondary_per_shard_scheduler|name=cronjob:geo_secondary_registry_consistency|name=cronjob:geo_sidekiq_cron_config|name=cronjob:historical_data|name=cronjob:issue_due_scheduler|name=cronjob:iterations_update_status|name=cronjob:ldap_all_groups_sync|name=cronjob:ldap_sync|name=cronjob:metrics_dashboard_schedule_annotations_prune|name=cronjob:pages_domain_ssl_renewal_cron|name=cronjob:pages_domain_verification_cron|name=cronjob:partition_creation|name=cronjob:personal_access_tokens_expiring|name=cronjob:prune_old_events|name=cronjob:prune_web_hook_logs|name=cronjob:remove_expired_group_links|name=cronjob:remove_unreferenced_lfs_objects|name=cronjob:sync_seat_link|name=cronjob:update_container_registry_info|name=cronjob:users_create_statistics|name=cronjob:vulnerabilities_statistics_schedule|name=delete_user|name=deployment:deployments_forward_deployment|name=elastic_full_index|name=elastic_indexing_control|name=elastic_namespace_indexer|name=elastic_namespace_rollout|name=geo:geo_batch_project_registry|name=geo:geo_batch_project_registry_scheduler|name=geo:geo_blob_verification_primary|name=geo:geo_container_repository_sync|name=geo:geo_design_repository_shard_sync|name=geo:geo_design_repository_sync|name=geo:geo_event|name=geo:geo_file_download|name=geo:geo_file_registry_removal|name=geo:geo_file_removal|name=geo:geo_hashed_storage_attachments_migration|name=geo:geo_hashed_storage_migration|name=geo:geo_project_sync|name=geo:geo_rename_repository|name=geo:geo_repositories_clean_up|name=geo:geo_repository_cleanup|name=geo:geo_repository_destroy|name=geo:geo_repository_shard_sync|name=geo:geo_repository_verification_primary_shard|name=geo:geo_repository_verification_primary_single|name=geo:geo_repository_verification_secondary_single|name=geo:geo_scheduler_primary_scheduler|name=geo:geo_scheduler_scheduler|name=geo:geo_scheduler_secondary_scheduler|name=geo:geo_secondary_repository_backfill|name=group_export|name=group_import|name=jira_connect:jira_connect_sync_branch|name=jira_connect:jira_connect_sync_merge_request|name=ldap_group_sync|name=mail_scheduler:mail_scheduler_issue_due|name=metrics_dashboard_prune_old_annotations|name=package_repositories:packages_nuget_extraction|name=personal_access_tokens:personal_access_tokens_groups_policy|name=personal_access_tokens:personal_access_tokens_instance_policy|name=phabricator_import_import_tasks|name=pipeline_background:ci_build_trace_chunk_flush|name=pipeline_background:ci_daily_build_group_report_results|name=pipeline_background:ci_pipeline_success_unlock_artifacts|name=pipeline_background:ci_ref_delete_unlock_artifacts|name=pipeline_creation:run_pipeline_schedule|name=pipeline_processing:ci_build_prepare|name=pipeline_processing:ci_resource_groups_assign_resource_from_resource_group|name=propagate_integration|name=repository_push_audit_event|name=service_desk_email_receiver|name=sync_seat_link_request|name=todos_destroyer:todos_destroyer_confidential_issue|name=todos_destroyer:todos_destroyer_entity_leave|name=todos_destroyer:todos_destroyer_group_private|name=todos_destroyer:todos_destroyer_private_features|name=todos_destroyer:todos_destroyer_project_private|name=unassign_issuables:members_destroyer_unassign_issuables|name=upload_checksum|name=vulnerabilities_statistics_adjustment|name=vulnerability_exports_export_deletion|name=chaos:chaos_cpu_spin|name=chaos:chaos_db_spin|name=chaos:chaos_kill|name=chaos:chaos_leak_mem|name=chaos:chaos_sleep|name=delete_stored_files|name=external_service_reactive_caching|name=object_storage:object_storage_background_move|name=object_storage:object_storage_migrate_uploads|name=analytics_code_review_metrics|name=self_monitoring_project_create|name=self_monitoring_project_delete|name=cronjob:import_software_licenses|name=refresh_license_compliance_checks|name=auto_devops:auto_devops_disable|name=gcp_cluster:cluster_configure_istio|name=gcp_cluster:cluster_install_app|name=gcp_cluster:cluster_patch_app|name=gcp_cluster:cluster_provision|name=gcp_cluster:cluster_update_app|name=gcp_cluster:cluster_upgrade_app|name=gcp_cluster:cluster_wait_for_app_update|name=gcp_cluster:cluster_wait_for_ingress_ip_address|name=gcp_cluster:clusters_applications_activate_service|name=gcp_cluster:clusters_applications_deactivate_service|name=gcp_cluster:clusters_applications_uninstall|name=gcp_cluster:clusters_cleanup_app|name=gcp_cluster:clusters_cleanup_project_namespace|name=gcp_cluster:clusters_cleanup_service_account|name=gcp_cluster:wait_for_cluster_creation|name=cronjob:ingress_modsecurity_counter_metrics|name=cronjob:network_policy_metrics|name=cronjob:pseudonymizer|name=propagate_service_template|name=file_hook|name=irker|name=project_service|name=web_hook|name=error_tracking_issue_link|name=incident_management:clusters_applications_check_prometheus_health|name=incident_management:incident_management_pager_duty_process_incident|name=incident_management:incident_management_process_alert|name=status_page_publish|name=github_import_advance_stage|name=github_importer:github_import_import_diff_note|name=github_importer:github_import_import_issue|name=github_importer:github_import_import_note|name=github_importer:github_import_import_pull_request|name=github_importer:github_import_refresh_import_jid|name=github_importer:github_import_stage_finish_import|name=github_importer:github_import_stage_import_base_data|name=github_importer:github_import_stage_import_issues_and_diff_notes|name=github_importer:github_import_stage_import_notes|name=github_importer:github_import_stage_import_pull_requests|name=github_importer:github_import_stage_import_repository|name=jira_importer:jira_import_advance_stage|name=jira_importer:jira_import_import_issue|name=jira_importer:jira_import_stage_finish_import|name=jira_importer:jira_import_stage_import_attachments|name=jira_importer:jira_import_stage_import_issues|name=jira_importer:jira_import_stage_import_labels|name=jira_importer:jira_import_stage_import_notes|name=jira_importer:jira_import_stage_start_import|name=project_import_schedule|name=dependency_proxy:purge_dependency_proxy_cache|name=epics:epics_update_epics_dates|name=create_evidence|name=create_commit_signature|name=create_note_diff_file|name=cronjob:admin_email|name=cronjob:authorized_project_update_periodic_recalculate|name=cronjob:repository_archive_cache|name=cronjob:repository_check_dispatch|name=cronjob:requests_profiles|name=cronjob:schedule_migrate_external_diffs|name=cronjob:stuck_merge_jobs|name=cronjob:trending_projects|name=cronjob:update_all_mirrors|name=cronjob:x509_issuer_crl_check|name=delete_diff_files|name=delete_merged_branches|name=detect_repository_languages|name=hashed_storage:hashed_storage_migrator|name=hashed_storage:hashed_storage_project_migrate|name=hashed_storage:hashed_storage_project_rollback|name=hashed_storage:hashed_storage_rollbacker|name=invalid_gpg_signature_update|name=merge_request_mergeability_check|name=migrate_external_diffs|name=project_daily_statistics|name=rebase|name=remote_mirror_notification|name=repository_check:repository_check_batch|name=repository_check:repository_check_clear|name=repository_check:repository_check_single_repository|name=repository_cleanup|name=repository_fork|name=repository_remove_remote|name=repository_update_remote_mirror|name=system_hook_push|name=update_namespace_statistics:namespaces_root_statistics|name=update_namespace_statistics:namespaces_schedule_aggregation|name=update_project_statistics|name=x509_certificate_revoke|name=cronjob:gitlab_usage_ping|name=mailers|name=git_garbage_collect|name=repository_import|name=repository_update_mirror|name=update_external_pull_requests
        resources:
          requests:
            cpu: 800m
            memory: 1G
          limits:
            cpu: 1.5
            memory: 2G
        extraEnv:
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"sidekiq\", \"stage\": \"main\", \"shard\": \"catchall\"}"
      - name: memory-bound
        concurrency: 1
        minReplicas: 1
        maxReplicas: 16
        nodeSelector:
          type: memory-bound
        podLabels:
          shard: memory-bound
        queues: resource_boundary=memory
        resources:
          requests:
            cpu: 50m
            memory: 650M
          limits:
            cpu: 2
            memory: 8G
        extraVolumeMounts: |
          - name: sidekiq-shared
            mountPath: /srv/gitlab/shared
            readOnly: false
        extraVolumes: |
          - name: sidekiq-shared
            emptyDir:
              sizeLimit: 50G
        extraEnv:
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"sidekiq\", \"stage\": \"main\", \"shard\": \"memory-bound\"}"
      # Run background migrations in their own shard
      - name: database-throttled
        concurrency: 5 # Discussion on this value in https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/merge_requests/276/diffs#note_367270352
        minReplicas: 1
        maxReplicas: 1
        nodeSelector:
          type: default
        podLabels:
          shard: database-throttled
        queues: feature_category=database&urgency=throttled
        resources:
          requests:
            cpu: 50m
            memory: 650M
          limits:
            cpu: 1.5
            memory: 6G
        extraEnv:
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"sidekiq\", \"stage\": \"main\", \"shard\": \"database-throttled\"}"
      # Run Gitaly Storage Migrations on their own shards
      # https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/436
      # Allow up to a maximum of 24 concurrent gitaly-throttled jobs
      - name: gitaly-throttled
        concurrency: 8
        minReplicas: 1
        maxReplicas: 3
        nodeSelector:
          type: default
        podLabels:
          shard: gitaly-throttled
        queues: feature_category=gitaly&urgency=throttled
        resources:
          requests:
            cpu: 50m
            memory: 650M
          limits:
            cpu: 1.5
            memory: 6G
        extraEnv:
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"sidekiq\", \"stage\": \"main\", \"shard\": \"gitaly-throttled\"}"
      - name: elasticsearch
        concurrency: 2
        minReplicas: 2
        maxReplicas: 2
        nodeSelector:
          type: default
        podLabels:
          shard: elasticsearch
        queues: feature_category=global_search&urgency=throttled
        resources:
          requests:
            cpu: 50m
            memory: 650M
          limits:
            cpu: 2
            memory: 8G
        extraEnv:
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"sidekiq\", \"stage\": \"main\", \"shard\": \"elasticsearch\"}"
      - name: low-urgency-cpu-bound
        concurrency: 5
        minReplicas: 2
        maxReplicas: 10
        nodeSelector:
          type: low-urgency-cpu-bound
        podLabels:
          shard: low-urgency-cpu-bound
        queues: resource_boundary=cpu&urgency=default,low
        resources:
          requests:
            cpu: 80m
            memory: 1G
          limits:
            cpu: 1.5
            memory: 6G
        extraEnv:
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"sidekiq\", \"stage\": \"main\", \"shard\": \"low-urgency-cpu-bound\"}"
      - name: urgent-cpu-bound
        concurrency: 5
        minReplicas: 1
        maxReplicas: 10
        nodeSelector:
          type: urgent-cpu-bound
        podLabels:
          shard: urgent-cpu-bound
        queues: resource_boundary=cpu&urgency=high&tags!=requires_disk_io
        resources:
          requests:
            cpu: 100m
            memory: 200M
          limits:
            cpu: 1
            memory: 2G
        extraEnv:
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"sidekiq\", \"stage\": \"main\", \"shard\": \"urgent-cpu-bound\"}"
      - name: urgent-other
        concurrency: 5
        minReplicas: 5
        maxReplicas: 10
        nodeSelector:
          type: urgent-other
        podLabels:
          shard: urgent-other
        queues: resource_boundary!=cpu&urgency=high
        resources:
          requests:
            cpu: 80m
            memory: 1G
          limits:
            cpu: 1.5
            memory: 6G
        extraEnv:
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"sidekiq\", \"stage\": \"main\", \"shard\": \"urgent-other\"}"

    registry:
      tokenIssuer: omnibus-gitlab-issuer
    trusted_proxies:
      - 10.0.0.0/8
  task-runner:
    enabled: false
  gitlab-monitor:
    enabled: false
  gitlab-exporter:
    enabled: false
  mailroom:
    enabled: {{ .Environment.Values.gitlab.mailroom.enabled }}
    nodeSelector:
      type: default
    podLabels:
      type: mailroom
      tier: sv
    image:
      repository: dev.gitlab.org:5005/gitlab/charts/components/images/gitlab-mailroom
      # Pin the tag to avoid following the chart default
      # https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/201
      tag: 0.0.6
      pullSecrets:
        - name: dev-registry-access-v1
    networkpolicy:
      enabled: true
      egress:
        enabled: true
        rules:
          # Allow DNS
          - to:
              - ipBlock:
                  cidr: 10.0.0.0/8
            ports:
              - port: 53
                protocol: UDP
              - port: 53
                protocol: TCP

          # Allow IMAP
          - to:
              - ipBlock:
                  cidr: 0.0.0.0/0
                  except:
                    - 10.0.0.0/8
            ports:
              - port: 993
                protocol: TCP

          # Allow Redis
          - to:
              - ipBlock:
                  cidr: 10.0.0.0/8
            ports:
              # pre Memorystore instance
              - port: 6379
                protocol: TCP

              # gstg, gprd
              - port: 26379
                protocol: TCP
