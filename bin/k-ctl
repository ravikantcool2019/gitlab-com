#!/usr/bin/env bash
# vim: ai:ts=2:sw=2:et

##############################
# wrapper script for running
# helm against this chart

set -euf -o pipefail
dir="$(
  cd "$(dirname "${0}")"
  pwd
)"

CHART="${CHART:-gitlab}"

export HELM_TILLER_LOGS=true
export HELM_TILLER_LOGS_DIR=${PWD}/tiller.log

# Source common functions and variable exports
# that are common to all charts

COMMON_SCRIPT_PATH="${COMMON_SCRIPT_PATH:-/k8s-workloads/common.bash}"

if [[ -r "$COMMON_SCRIPT_PATH" ]]; then
  source "$COMMON_SCRIPT_PATH"
else
  # Grab the CI image version from  the .gitlab-ci.yml
  _BBLK="\\033[1;30m"
  _NORM="\\033[0m"
  version=$(sed -E -e "s/[[:space:]]+CI_IMAGE_VERSION:[[:space:]]'(v[0-9\\.]+)'/\\1/" -e "t" -e "d" .gitlab-ci.yml)
  echo -e "${_BBLK}Sourcing version $version of the common shell script"
  echo -e "if you want to use a local version set COMMON_SCRIPT_PATH to the location of common.bash"
  echo -e "or update .gitlab-ci.yml to set a new version${_NORM}"
  echo ""
  source <(curl -s "https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/common/raw/$version/bin/common.bash")
fi

NAME="${NAME:-gitlab${STAGE_SUFFIX}}"
NAMESPACE="${NAMESPACE:-gitlab${STAGE_SUFFIX}}"
CHART_VERSION=$(get_chart_version)
export CHART_VERSION
CHARTS_DIR=$(mktemp -d)
MANIFESTS_DIR="$dir/../manifests"

is_semver() {
  if [[ $1 =~ ^[0-9]+\.[0-9]+\.[0-9]+ ]]; then
    return 0
  else
    return 1
  fi
}

environment=${ENV}${STAGE_SUFFIX}
helmfile_common_options=(
  "--log-level" "${LOG_LEVEL:-info}"
  "--environment" "${environment}"
)

helmfile_cmd_options=(
  "--concurrency" "1"
  "--skip-deps"
  "--suppress-secrets"
)

helm_diff() {
  helmfile "${helmfile_common_options[@]}" diff "${helmfile_cmd_options[@]}" --args '--context 10'
}

case "$ACTION" in
  install | upgrade)
    overview

    helmfile "${helmfile_common_options[@]}" repos

    if [[ ${dry_run:-} == "true" ]]; then
      debug "-- Helm Diff --" "${_CYN}"
      helm_diff | tee /tmp/helm-diff
      # Send a diff notification, allowed to
      # fail so we don't block the pipeline
      # Skip the notifications for auto-deploy triggered pipelines since they are not
      # done in the context of an MR.
      if [[ ${AUTO_DEPLOY:-false} == "false" && -f /k8s-workloads/notify-mr ]]; then
        /k8s-workloads/notify-mr -d /tmp/helm-diff -e "$environment" || echo "WARNING: notify-mr diff notification failed"
      fi
      debug "-------" "${_CYN}"
    else
      tail --pid=$$ --retry --follow "${HELM_TILLER_LOGS_DIR}" &

      helmfile "${helmfile_common_options[@]}" apply "${helmfile_cmd_options[@]}"
    fi
    ;;

  list)
    # `helmfile` outputs less information for this command
    # since `helm` does better, let's leave that in place
    debug "-- Helm List --" "${_CYN}"
    helm tiller run helm list
    ;;

  remove)
    overview
    warn_removal

    # Remove secrets by removing gitlab-secrets release via helmfile
    if [[ ${dry_run:-} != "true" ]]; then
      helmfile "${helmfile_common_options[@]}" destroy
    else
      debug "Would run: helmfile ${helmfile_common_options[*]} destroy"
    fi
    ;;

  template)
    overview

    helmfile "${helmfile_common_options[@]}" template --output-dir "$MANIFESTS_DIR" --concurrency 1
    ;;
esac

set +x
rm -rf "${CHARTS_DIR:?}"
